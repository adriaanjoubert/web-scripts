// ==UserScript==
// @name         General
// @match        *://*/*
// @description  .
// @version      1
// ==/UserScript==

(function() {
    'use strict';

    const allowCopyAndPaste = function(e) {
        e.stopImmediatePropagation();
        return true;
    };

    document.addEventListener('copy', allowCopyAndPaste, true);
    document.addEventListener('paste', allowCopyAndPaste, true);

    function addGlobalStyle(css) {
        var head, style;
        head = document.getElementsByTagName('head')[0];
        if (!head) { return; }
        style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = css;
        head.appendChild(style);
    }

    addGlobalStyle('* { user-select: auto !important; }');
})();
